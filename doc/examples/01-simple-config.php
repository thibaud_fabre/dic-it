<?php

    require_once 'vendor/autoload.php';

    $config = \DICIT\Config\ConfigFactory::fromFile(__DIR__ . '/01-simple-config.json');
    $container = \DICIT\ContainerFactory::create($config);

    $obj = $container->get('SimpleObject');

    echo $obj->property . PHP_EOL;
