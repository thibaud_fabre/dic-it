<?php

namespace DICIT\Tests\Config;

use DICIT\Config\ConfigFactory;
use DICIT\Config\Json;
use DICIT\Config\PHP;
use DICIT\Config\YML;

/**
 * @todo use VFS for files
 * @author thibaud
 *
 */

class ConfigFactoryTest extends \PHPUnit_Framework_TestCase
{

    public function getInvalidPaths()
    {
        /* @f:off */
        return array(
            array('some-file.phpc'),
            array('some-file.c'),
            array('some-file.doc'),
            array('some-file.xls'),
            array('some-file.png'),
            array('some-file.jpg')
        );
        /* @f:on */
    }

    /**
     * @dataProvider getInvalidPaths
     * @expectedException \InvalidArgumentException
     */
    public function testThrowsExceptionWhenTypeIsUnknown($path)
    {
        $config = ConfigFactory::fromFile('tests/res/config-factory-test-files/' . $path);
    }

    /**
     * @dataProvider getInvalidPaths
     * @expectedException \InvalidArgumentException
     */
    public function testThrowsExceptionWhenFileDoesNotExist($path)
    {
        $config = ConfigFactory::fromFile($path);
    }

    public function getValidPaths()
    {
        /* @f:off */
        return array(
            array('config.json', '\DICIT\Config\Json'),
            array('config.yaml', '\DICIT\Config\YML'),
            array('config.yml', '\DICIT\Config\YML'),
            array('config.php', '\DICIT\Config\PHP')
        );
        /* @f:on */
    }

    /**
     * @dataProvider getValidPaths
     */
    public function testReturnsConfigWhenFileTypeIsKnown($path, $class)
    {
        $this->assertInstanceOf($class,ConfigFactory::fromFile('tests/res/config-factory-test-files/' . $path));
    }
}

