<?php

namespace DICIT\Tests\Config\Parser;

abstract class AbstractParserTest extends \PHPUnit_Framework_TestCase
{
    
    protected function getData($name)
    {
        return file_get_contents(getcwd() . '/tests/res/config-parser-test-files/' . $name);
    }
    
}