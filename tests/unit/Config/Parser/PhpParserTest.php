<?php

namespace DICIT\Tests\Config\Parser;

use DICIT\Config\Parser\PhpParser;
class PhpParserTest extends \PHPUnit_Framework_TestCase
{
    public function testParsePhp()
    {
        $php = <<<EOC
            return [ 'test' => 'test-value' ];    
EOC;
        
        $parser = new PhpParser();
        
        $array = $parser->parse($php);
        
        $this->assertEquals('test-value', $array['test']);
    }
}