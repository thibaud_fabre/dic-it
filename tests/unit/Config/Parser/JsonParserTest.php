<?php

namespace DICIT\Tests\Config\Parser;

use DICIT\Config\Parser\JsonParser;
use DICIT\ArrayResolver;
class JsonParserTest extends AbstractParserTest
{

    public function testParse()
    {
    	$data = $this->getData('config.json');
    	$parser = new JsonParser();
    	
    	$config = $parser->parse($data);
    	
    	$this->assertArrayHasKey('params', $config);
    	$this->assertArrayHasKey('classes', $config);
    }
    
    /**
     * @expectedException \Exception
     */
    public function testParseThrowsExceptionOnInvalidData()
    {
        $data = '{\/}';
        $parser = new JsonParser();
        
        $config = $parser->parse($data);
    }
    
    public function testUnparseReturnsParsableData()
    {
        $data = $this->getData('config.json');
        $parser = new JsonParser();
         
        $config = $parser->parse($data);
        $data = $parser->unparse($config);
        $config = $parser->parse($data);
         
        $this->assertArrayHasKey('params', $config);
        $this->assertArrayHasKey('classes', $config);
    }
}