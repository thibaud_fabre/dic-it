<?php
namespace DICIT\Tests;

use DICIT\ActivatorFactory;
use DICIT\Activators\DefaultActivatorFactory;

class ActivatorFactoryTest extends \PHPUnit_Framework_TestCase
{
    
    private $factory;
    
    protected function setUp()
    {
        $this->factory = new DefaultActivatorFactory();
    }

    public function getInvalidServiceConfigurations()
    {
        return array(
            array(array()),
            array(array('class' => '\DummyClass', 'builder' => '\invalidBuilderDefinition'))
        );
    }

    /**
     * @dataProvider getInvalidServiceConfigurations
     * @expectedException \DICIT\UnbuildableServiceException
     */
    public function testGetActivatorThrowsExceptionForInvalidConfigurations($serviceConfig)
    {
        $this->factory->getActivator('myService', $serviceConfig);
    }

    public function testGetActivatorWithStaticBuilderConfigReturnsStaticActivator()
    {
        $serviceConfig = array('class' => '\DummyClass', 'builder' => '\DummyBuilder::dummyFactoryMethod');

        $this->assertInstanceOf('\DICIT\Activators\StaticInvocationActivator',
            $this->factory->getActivator('myService', $serviceConfig));
    }

    public function testGetActivatorWithInstanceBuilderConfigReturnsInstanceActivator()
    {
        $serviceConfig = array('class' => '\DummyClass', 'builder' => '@DummyBuilder->dummyFactoryMethod');

        $this->assertInstanceOf('\DICIT\Activators\InstanceInvocationActivator',
            $this->factory->getActivator('myService', $serviceConfig));
    }

    public function testGetActivatorWithRemoteConfigReturnsRemoteActivator()
    {
        $serviceConfig = array('class' => '\DummyClass', 'remote' => array());

        $this->assertInstanceOf('\DICIT\Activators\RemoteActivator',
            $this->factory->getActivator('myService', $serviceConfig));
    }
}
