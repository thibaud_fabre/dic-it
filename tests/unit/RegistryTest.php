<?php

namespace DICIT\Tests;

use DICIT\Registry;

class RegistryTest extends \PHPUnit_Framework_TestCase
{

    public function testFlushClearsRegistry()
    {
        $registry = new Registry();
        $value = 'value';

        $registry->set('key', $value);
        $registry->flush();

        $this->assertNull($registry->get('key'));
    }

    /**
     * @expectedException \RuntimeException
     */
    public function testGetThrowsExceptionWithExceptionFlagOn()
    {
        $registry = new Registry();

        $registry->getStrict('unknownKey');
    }

    public function testGetReturnsNullWithExceptionFlagOff()
    {
        $registry = new Registry();

        $this->assertNull($registry->get('unknownKey'));
    }

    public function testGetReturnsSetValue()
    {
        $registry = new Registry();
        $value = 'myValue';

        $registry->set('key', $value);

        $this->assertEquals($value, $registry->get('key'));
    }

    public function testRegistryStoresCorrectObjectReferences()
    {
        $registry = new Registry();
        $value = new \stdClass();

        $registry->set('key', $value);

        $value->property = 'modified';

        $this->assertEquals('modified', $registry->get('key')->property);
        $this->assertSame($value, $registry->get('key'));
    }

}
