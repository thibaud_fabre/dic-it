<?php

namespace DICIT\Tests;

use DICIT\ReferenceResolver;
use DICIT\ContainerFactory;

class ConfigTest extends \PHPUnit_Framework_TestCase
{
    public function testEnvironmentVariablesAreCorrectlyExpandedInIncludes()
    {
        $container = ContainerFactory::createFromYaml(__DIR__ . '/../res/config.yml');

        $this->assertEquals(getenv('HOME'), $container->getParameter('test'));
        $this->assertEquals('$env.HOME', $container->getParameter('safe'));
    }
}
