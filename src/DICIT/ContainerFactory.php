<?php

namespace DICIT;

use DICIT\Activators\DefaultActivatorFactory;
use DICIT\Activators\DeferredActivatorFactory;
use DICIT\Config\AbstractConfig;
use DICIT\Config\ConfigFactory;
use DICIT\Config\YMLInline;

/**
 * Factory class to build dependency injection containers from a configuration file.
 *
 * @author thibaud
 */
class ContainerFactory
{

    /**
     * Creates a new container using the given JSON configuration file.
     *
     * @param string $file Path to the JSON configuration file.
     * @param array $options
     *
     * @return Container
     */
    public static function createFromJson($file, array $options = array())
    {
        return self::createInstance($file, $options);
    }

    /**
     * Creates a new container using the given PHP configuration file.
     *
     * @param string $file Path to the PHP configuration file.
     * @param array $options
     *
     * @return Container
     */
    public static function createFromPhp($file, array $options = array())
    {
        return self::createInstance($file, $options);
    }

    /**
     * Creates a new container using the given YAML configuration file.
     *
     * @param string $file Path to the YAML configuration file.
     * @param array $options
     *
     * @return Container
     */
    public static function createFromYaml($file, array $options = array())
    {
        return self::createInstance($file, $options);
    }

    /**
     * Creates a new container using the given Yaml definition.
     *
     * @param string $yaml The inline Yaml definition.
     * @param array $options
     *
     * @return Container
     */
    public static function createFromInlineYaml($yaml, array $options = array())
    {
        return self::createInstance(new YMLInline($yaml), $options);
    }

    /**
     * Creates a new container using the given configuration.
     *
     * @param AbstractConfig|string $config An instance of AbstractConfig or the path of a configuration file.
     * @param array $options
     *
     * @return Container
     */
    public static function create($config, array $options = array())
    {
        return self::createInstance($config, $options);
    }

    private static function createInstance($config, array $options)
    {
        if (is_string($config)) {
            $config = self::getAbstractConfig($config);
        }

        if (! ($config instanceof AbstractConfig)) {
            throw new \InvalidArgumentException(
                'Invalid configuration : must be an instance of AbstractConfig, a config file path, or valid inline PHP.');
        }

        $resolver = new ArrayResolver($options);
        $activatorFactory = new DefaultActivatorFactory();

        if ((bool)$resolver->resolve('deferred', false)) {
            $activatorFactory = new DeferredActivatorFactory($activatorFactory);
        }

        $pipeline = new ServiceBuilder(
            $activatorFactory,
            new InjectorFactory(),
            new EncapsulatorFactory()
        );

        return new DefaultContainer($config, $pipeline);
    }

    /**
     * @param string $config
     */
    private static function getAbstractConfig($config)
    {
        if (! file_exists($config)) {
            throw new \InvalidArgumentException('Config not found.');
        }

        $factory = new ConfigFactory();

        return $factory->fromFile($config);
    }
}
