<?php

namespace DICIT;

use DICIT\Config\AbstractConfig;
use DICIT\Config\TemplatedConfigProcessor;

class DefaultContainer implements Container
{

    /**
     *
     * @var ArrayResolver
     */
    protected $config = null;

    /**
     *
     * @var ParameterContainer
     */
    protected $parameterContainer;

    /**
     *
     * @var ArrayResolver
     */
    protected $classes;

    /**
     *
     * @var Registry
     */
    protected $registry = null;

    /**
     *
     * @var ServiceBuilder
     */
    protected $serviceBuilder;

    /**
     *
     * @var ReferenceResolver
     */
    protected $referenceResolver = null;

    /**
     *
     * @param AbstractConfig $config
     */
    public function __construct(AbstractConfig $config, ServiceBuilder $builder)
    {
        $templateProcessor = new TemplatedConfigProcessor();
        $configData = $config->load();

        $this->serviceBuilder = $builder;
        $this->config = $templateProcessor->process(new ArrayResolver($configData));

        $this->registry = new Registry();
        $this->referenceResolver = new DefaultReferenceResolver($this);

        $parameters = $this->config->resolve('parameters', array());
        $this->parameterContainer = new ParameterContainer($this, $parameters);

        $this->classes = $this->config->resolve('classes', array());
    }

    public function build($definition, $serviceName = null)
    {
        if ($serviceName === null) {
            $serviceName = md5(var_export($definition, true) . rand());
        }

        return $this->serviceBuilder->buildService($this, $serviceName, $definition);
    }

    /**
     * Binds an existing object or an object definition to a key in the container.
     *
     * @param string $key
     *            The key to which the new object/definition should be bound.
     * @param mixed $item
     *            An array or an object to bind.
     *            If $item is an object, it will be registered as a singleton in the
     *            object registry. Otherwise, $item will be handled as an object definition.
     */
    public function bind($key, $item)
    {
        if (is_array($item)) {
            return $this->classes[$key] = $item;
        }

        return $this->registry->set($key, $item);
    }

    /**
     * @param string $key
     */
    public function lateBind($key, & $item)
    {
        if (is_array($item)) {
            return $this->classes[$key] = & $item;
        }

        return $this->registry->rset($key, $item);
    }

    /**
     * Set a parameter in the container on any key
     *
     * @param string $key
     * @param mixed $value
     */
    public function setParameter($key, $value)
    {
        $this->parameterContainer->set($key, $value);

        return $this;
    }

    /**
     * Retrieve the parameter value configured in the container
     *
     * @param string $parameterName
     * @return mixed
     */
    public function getParameter($parameterName)
    {
        return $this->parameterContainer->get($parameterName);
    }

    /**
     * Retrieve a class configured in the container
     *
     * @param string $serviceName
     * @return object
     */
    public function & get($serviceName)
    {
        if ($this->registry->has($serviceName)) {
            $service = $this->registry->getStrict($serviceName);

            return $service;
        }

        $serviceConfig = $this->classes->resolve($serviceName, null);

        if ($serviceConfig == null) {
            throw new UnknownDefinitionException($serviceName);
        }

        try {
            $service = $this->loadService($serviceName, $serviceConfig->extract());
        } catch (UnknownDefinitionException $ex) {
            throw new \RuntimeException(sprintf("Dependency '%s' not found while trying to build '%s'.",
                $ex->getServiceName(), $serviceName));
        }

        return $service;
    }

    /**
     * (non-PHPdoc)
     * @see \DICIT\Container::getNamespace()
     */
    public function getNamespace($namespace)
    {
        $classes = [];
        $definitions = $this->classes->extract();

        foreach (array_keys($definitions) as $class) {
            if (strpos($class, $namespace, 0) === 0) {
                $classes[$class] = $this->get($class);
            }
        }

        return $classes;
    }

    /**
     *
     * @return ArrayResolver
     */
    public function getGlobalConfig()
    {
        return $this->config;
    }

    /**
     * Resolves the value of a reference key.
     *
     * @param string $reference
     * @return mixed
     */
    public function resolve($reference)
    {
        return $this->referenceResolver->resolve($reference);
    }

    /**
     * Resolves an array of references.
     *
     * @param array $references
     * @return array containing all the resolved references
     */
    public function resolveMany(array $references)
    {
        return $this->referenceResolver->resolveMany($references);
    }

    /**
     * Flush the registry
     *
     * @return Container
     */
    public function flushRegistry()
    {
        $this->registry->flush();
        return $this;
    }

    /**
     * Chain of command of the class loader
     *
     * @param array $serviceConfig
     * @param string $serviceName
     * @return object
     */
    protected function loadService($serviceName, $serviceConfig)
    {
        return $this->serviceBuilder->buildService($this, $serviceName, $serviceConfig);
    }
}
