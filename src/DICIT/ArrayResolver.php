<?php

namespace DICIT;

class ArrayResolver extends Iterator
{

    public function __construct(array & $source = null)
    {
        if ($source == null) {
            $source = array();
        }

        $this->source = & $source;
    }
    
    public function current()
    {
        return $this->wrapIfNecessary(parent::current());
    }

    public function extract()
    {
        return $this->source;
    }

    /**
     * Resolves a value stored in an array, optionally by using dot notation to access nested elements.
     *
     * @param string $key The key value to resolve.
     * @param mixed $default
     * @return ArrayResolver|mixed The resolved value or the provided default value. If the resolved value is an array, it will be wrapped as an ArrayResolver instance.
     */
    public function resolve($key, $default = null)
    {
        $toReturn = $default;
        $dotted = explode(".", $key);

        if (count($dotted) > 1) {
            $toReturn = $this->walkNameComponents($dotted, $default);
        }
        elseif (array_key_exists($key, $this->source)) {
            $toReturn = $this->source[$key];
        }

        return $this->wrapIfNecessary($toReturn);
    }

    private function walkNameComponents(array $dotted, $default)
    {
        $currentDepthData = $this->source;

        foreach ($dotted as $paramKey) {
            if (! array_key_exists($paramKey, $currentDepthData)) {
                return $default;
            }

            $currentDepthData = $currentDepthData[$paramKey];
        }

        return $currentDepthData;
    }

    private function wrapIfNecessary($value)
    {
        if (is_array($value)) {
            return new static($value);
        }

        return $value;
    }
}
