<?php
namespace DICIT\Activators;

use DICIT\Activator;
use DICIT\Container;
use DICIT\UnbuildableServiceException;

class DefaultActivator implements Activator
{

    public function createInstance(Container $container, $serviceName, array $serviceConfig)
    {
        $className = $serviceConfig['class'];

        if (! class_exists($className)) {
            throw new UnbuildableServiceException(sprintf("Class '%s' not found.", $className));
        }

        $class = new \ReflectionClass($className);
        $activationArgs = array();

        if (isset($serviceConfig['arguments'])) {
            if (! is_array($serviceConfig['arguments'])) {
                $serviceConfig['arguments'] = [ $serviceConfig['arguments'] ];
            }
            $activationArgs = $container->resolveMany($serviceConfig['arguments']);
        }

        return $class->newInstanceArgs($activationArgs);
    }
}
