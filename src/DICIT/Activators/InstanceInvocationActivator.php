<?php
namespace DICIT\Activators;

use DICIT\Activator;
use DICIT\Container;
use DICIT\UnbuildableServiceException;
use DICIT\Util\MethodNameParser;

class InstanceInvocationActivator implements Activator
{

    private $methodNameParser;

    public function __construct()
    {
        $this->methodNameParser = new MethodNameParser();
    }

    public function createInstance(Container $container, $serviceName, array $serviceConfig)
    {
        $method = $this->methodNameParser->getMethodInvocation($serviceConfig['builder']);
        $invocationSite = $container->get($method->getOwner());

        $activationArgs = $method->hasArguments()
            ? $container->resolveMany($method->getArguments())
            : isset($serviceConfig['arguments'])
                ? $container->resolveMany($serviceConfig['arguments'])
                : array();

        if (! method_exists($invocationSite, $method->getName())) {
            throw new UnbuildableServiceException(
                sprintf("Instance '%s' (%s) has no '%s' method.", $method->getOwner(), get_class($invocationSite), $method->getName()));
        }

        return call_user_func_array(array($invocationSite, $method->getName()), $activationArgs);
    }
}
