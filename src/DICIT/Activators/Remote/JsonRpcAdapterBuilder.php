<?php

namespace DICIT\Activators\Remote;

use ProxyManager\Factory\RemoteObject\Adapter\JsonRpc;
use Zend\Json\Server\Client;

class JsonRpcAdapterBuilder implements AdapterBuilder
{

    public function accepts($protocol)
    {
        return $protocol == 'json-rpc';
    }

    public function build($endpoint)
    {
        return new JsonRpc(new Client($endpoint));
    }
}
