<?php

namespace DICIT\Activators;

use DICIT\Activators\Remote\JsonRpcAdapterBuilder;
use DICIT\Activators\Remote\RestAdapterBuilder;
use DICIT\Activators\Remote\SoapAdapterBuilder;
use DICIT\Activators\Remote\XmlRpcAdapterBuilder;
use ProxyManager\Factory\RemoteObject\AdapterInterface;

class RemoteAdapterFactory
{

    private $builders = array();

    public function __construct()
    {
        $this->builders[] = new JsonRpcAdapterBuilder();
        $this->builders[] = new RestAdapterBuilder();
        $this->builders[] = new SoapAdapterBuilder();
        $this->builders[] = new XmlRpcAdapterBuilder();
    }

    /**
     *
     * @param string $serviceName
     * @param array $serviceConfig
     * @throws UnknownProtocolException
     * @return AdapterInterface
     */
    public function getAdapter($serviceName, array $serviceConfig)
    {
        $this->validateConfig($serviceConfig, $serviceName);

        $protocol = $serviceConfig['protocol'];
        $endpoint = $serviceConfig['endpoint'];

        foreach ($this->builders as $builder) {
            if ($builder->accepts($protocol)) {
                return $builder->build($endpoint);
            }
        }

        throw new UnknownProtocolException(sprintf("Protocol '%s' is not supported ", $protocol));
    }

    /**
     *
     * @param string $serviceName
     */
    private function validateConfig($serviceConfig, $serviceName)
    {
        if (! isset($serviceConfig['protocol']) || ! isset($serviceConfig['endpoint'])) {
            throw new \InvalidArgumentException(
                sprintf("Protocol and endpoint are required for remote object '%s'", $serviceName));
        }
    }
}
