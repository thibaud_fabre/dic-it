<?php

namespace DICIT\Activators;

use DICIT\Activator;
use DICIT\ActivatorFactory;
use DICIT\UnbuildableServiceException;
use DICIT\Util\MethodNameParser;

class DefaultActivatorFactory implements ActivatorFactory
{

    private $activators = array();

    private $methodNameParser;

    public function __construct()
    {
        $this->addActivator('default', new DefaultActivator());
        $this->addActivator('builder-static', new StaticInvocationActivator());
        $this->addActivator('builder', new InstanceInvocationActivator());
        $this->addActivator('remote', new RemoteActivator(new RemoteAdapterFactory()));

        $this->methodNameParser = new MethodNameParser();
    }

    /**
     *
     * @param string $key
     */
    public function addActivator($key, Activator $activator)
    {
        $this->activators[$key] = $activator;
    }

    /**
     *
     * @param string $serviceName
     * @param array $configuration
     * @throws UnbuildableServiceException
     * @return Activator
     */
    public function getActivator($serviceName, array $configuration)
    {
        $activator = $this->selectActivator($configuration);

        if ($activator == null) {
            throw new UnbuildableServiceException(sprintf("Unbuildable service : '%s', no suitable activator found.", $serviceName));
        }

        return $activator;
    }

    private function selectActivator($configuration)
    {
        $activator = null;

        if (array_key_exists('builder', $configuration)) {
            $activator = $this->getActivatorByBuilderType($configuration);
        } elseif (array_key_exists('class', $configuration)) {
            $activator = $this->getRemoteOrDefaultActivator($configuration);
        }

        return $activator;
    }

    private function getActivatorByBuilderType($configuration)
    {
        if ($builderType = $this->getBuilderType($configuration['builder'])) {
            return $this->activators[$builderType];
        }

        return null;
    }

    private function getBuilderType($builderKey)
    {
        if ($this->methodNameParser->isStaticInvocation($builderKey)) {
            return 'builder-static';
        }

        if ($this->methodNameParser->isInstanceInvocation($builderKey)) {
            return 'builder';
        }

        return null;
    }

    private function getRemoteOrDefaultActivator($configuration)
    {
        if (array_key_exists('remote', $configuration)) {
            return $this->activators['remote'];
        }

        return $this->activators['default'];
    }
}
