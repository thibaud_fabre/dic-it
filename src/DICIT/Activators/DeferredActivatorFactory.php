<?php

namespace DICIT\Activators;

use DICIT\Activator;
use DICIT\ActivatorFactory;

class DeferredActivatorFactory implements ActivatorFactory
{
    private $factory;

    public function __construct(ActivatorFactory $factory)
    {
        $this->factory = $factory;
    }

    function addActivator($key, Activator $activator)
    {
        $this->factory->addActivator($key, new LazyActivator($activator));
    }

    function getActivator($serviceName, array $configuration)
    {
        return $this->factory->getActivator($serviceName, $configuration);
    }
}
