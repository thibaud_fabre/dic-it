<?php

namespace DICIT;

use DICIT\Resolver\ConstantResolver;
use DICIT\Resolver\ContainerResolver;
use DICIT\Resolver\DynamicParameterResolver;
use DICIT\Resolver\EnvironmentVariableResolver;
use DICIT\Resolver\NamespaceResolver;
use DICIT\Resolver\ParameterResolver;
use DICIT\Resolver\PassThroughResolver;
use DICIT\Resolver\ServiceResolver;

class DefaultReferenceResolver implements ReferenceResolver
{

    const CONTAINER_REGEXP = '`^\$container$`i';

    const ENVIRONMENT_REGEXP = '`^\$env\.(.*)$`i';

    const CONSTANT_REGEXP = '`^\$const\.(.*)$`i';

    /**
     *
     * @var Container
     */
    private $container;

    private $resolvers = array();

    /**
     * Create a new resolver.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;

        $this->resolvers[] = new DynamicParameterResolver($this);
        $this->resolvers[] = new NamespaceResolver($container);
        $this->resolvers[] = new ParameterResolver($container);
        $this->resolvers[] = new ServiceResolver($container);
        $this->resolvers[] = new ContainerResolver($container, self::CONTAINER_REGEXP);
        $this->resolvers[] = new EnvironmentVariableResolver(self::ENVIRONMENT_REGEXP);
        $this->resolvers[] = new ConstantResolver(self::CONSTANT_REGEXP);
        $this->resolvers[] = new PassThroughResolver();
    }

    /**
     * Return the resolved value of the given reference.
     *
     * @param mixed $reference
     * @return mixed
     */
    public function resolve($reference)
    {
        if ((is_object($reference) || is_array($reference))) {
            return $this->container->build($reference);
        }

        return $this->resolveInternal($reference);
    }

    /**
     * @param string $reference
     * @return mixed
     */
    private function resolveInternal($reference)
    {
        foreach ($this->resolvers as $resolver) {
            if ($resolver->accepts($reference)) {
                return $resolver->resolve($reference);
            }
        }

        throw new UnknownDefinitionException($reference);
    }

    /**
     * Resolves an array of references
     *
     * @param array $references
     * @return mixed
     */
    public function resolveMany(array $references)
    {
        $convertedParameters = array();

        foreach ($references as $reference) {
            $convertedValue = $this->resolve($reference);
            $convertedParameters[] = $convertedValue;
        }

        return $convertedParameters;
    }
}
