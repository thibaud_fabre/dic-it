<?php

namespace DICIT\Config;

class InlineConfig extends AbstractConfig
{

    private $parser;

    private $content;

    public function __construct(Parser $parser, $configText)
    {
        $this->parser = $parser;
        $this->content = $configText;
    }

    protected function doLoad()
    {
        $this->data = $this->parser->parse($this->content);
    }
}
