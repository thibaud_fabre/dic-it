<?php

namespace DICIT\Config\Parser;

use DICIT\Config\Parser;

class PhpParser implements Parser
{

    /**
     * (non-PHPdoc)
     * @see \DICIT\Config\Parser::parse()
     * @SuppressWarnings(PHPMD.EvalExpression)
     */
    public function parse($string)
    {
        return eval($string);
    }

    public function unparse(array $data)
    {
        return var_export($data, true);
    }
}
