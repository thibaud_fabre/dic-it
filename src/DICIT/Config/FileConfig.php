<?php

namespace DICIT\Config;

use DICIT\Util\Arrays;

class FileConfig extends AbstractConfig
{

    private $arrayUtil;

    private $parser;

    private $sourceFile;

    public function __construct(Parser $parser, $sourceFile)
    {
        if (! file_exists($sourceFile)) {
            throw new \InvalidArgumentException('Configuration file \'' . $sourceFile . '\' not found.');
        }

        $this->sourceFile = $sourceFile;
        $this->parser = $parser;
        $this->arrayUtil = new Arrays();
    }

    public function getSourceFile()
    {
        return $this->sourceFile;
    }

    protected function loadFile($filePath)
    {
        $data = file_get_contents($filePath);

        try {
            $data = $this->parser->parse($data);
        }
        catch (\Exception $ex) {
            throw new InvalidConfigurationException('Invalid file : ' . $filePath);
        }

        return $data;
    }

    protected function doLoad()
    {
        $data = $this->getSourcedData($this->sourceFile);
        $dirname = dirname($this->sourceFile);

        if (array_key_exists('include', $data)) {
            foreach ($data['include'] as $relativeFilePath) {
                $file = $dirname . DIRECTORY_SEPARATOR . $relativeFilePath;
                $data = $this->arrayUtil->mergeRecursiveUnique($data, $this->getSourcedData($file));
            }
        }

        return $data;
    }

    private function getSourcedData($file)
    {
        $data = $this->objectToArray($this->loadFile($file));

        if (! is_array($data)) {
            throw new InvalidConfigurationException('Invalid configuration, data is not an array.');
        }

        // Store source file of current node
        // $data['__META__'] = array('source' => $file);

        return $data;
    }

    private function objectToArray($mixed)
    {
        if (! is_object($mixed)) {
            return $mixed;
        }

        $array = array();

        foreach ($mixed as $property => $value) {
            $array[$property] = $this->objectToArray($value);
        }

        return $array;
    }
}
