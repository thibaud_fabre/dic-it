<?php

namespace DICIT\Config;

use Symfony\Component\Yaml\Yaml;

class YMLInline extends AbstractConfig
{

    protected $parser;

    protected $inline = '';

    protected $data = array();

    /**
     * @param string $string
     */
    public function __construct($string)
    {
        $this->inline = $string;
        $this->parser = new Yaml();
    }

    protected function doLoad()
    {
        return $this->parser->parse($this->inline);
    }

    public function compile()
    {
        $ret = $this->load();
        $dump = var_export($ret, true);
        return $dump;
    }
}
