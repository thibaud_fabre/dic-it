<?php
namespace DICIT\Config;

use DICIT\Config\Parser\PhpParser;

final class PHP extends FileConfig
{
    public function __construct($sourceFile)
    {
        parent::__construct(new PhpParser(), $sourceFile);
    }
}
