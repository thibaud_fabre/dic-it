<?php
namespace DICIT\Config;

use DICIT\Config\Parser\JsonParser;

final class Json extends FileConfig
{
    public function __construct($sourceFile)
    {
        parent::__construct(new JsonParser(), $sourceFile);
    }
}
