<?php

namespace DICIT\Config;

class ConfigFactory
{

    public static function fromFile($path)
    {
        if (! file_exists($path)) {
            throw new \InvalidArgumentException("File not found : " . $path);
        }

        $extension = substr($path, strrpos($path, '.') + 1);
        $types = array(
            'json' => '\DICIT\Config\Json',
            'php' => '\DICIT\Config\PHP',
            'yml' => '\DICIT\Config\YML',
            'yaml' => '\DICIT\Config\YML'
        );

        if (array_key_exists($extension, $types)) {
            return new $types[$extension]($path);
        }

        throw new \InvalidArgumentException('Unable to detect file type : ' . $path);
    }
}
