<?php
namespace DICIT\Config;

use DICIT\Config\Parser\YamlParser;

final class YML extends FileConfig
{
    public function __construct($sourceFile)
    {
        parent::__construct(new YamlParser(), $sourceFile);
    }
}
