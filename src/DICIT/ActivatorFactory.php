<?php

namespace DICIT;

interface ActivatorFactory
{

    /**
     *
     * @param string $key
     * @return void
     */
    function addActivator($key, Activator $activator);

    /**
     *
     * @param string $serviceName
     * @param array $configuration
     * @throws UnbuildableServiceException
     * @return Activator
     */
    function getActivator($serviceName, array $configuration);
}
