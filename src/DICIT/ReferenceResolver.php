<?php

namespace DICIT;

interface ReferenceResolver
{
    public function resolve($reference);

    public function resolveMany(array $references);
}
