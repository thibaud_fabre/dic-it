<?php

namespace DICIT\Injectors;

use DICIT\Container;
use DICIT\Injector;

class MethodInjector implements Injector
{

    private $methodInvoker;

    public function __construct()
    {
        $this->methodInvoker = new MethodInvoker();
    }

    public function inject(Container $container, $service, array $serviceConfig)
    {
        $callConfig = array();

        if (array_key_exists('call', $serviceConfig)) {
            $callConfig = $serviceConfig['call'];
        }

        foreach($callConfig as $methodName => $parameters) {
            $this->methodInvoker->invoke($container, $service, $methodName, $parameters);
        }

        return true;
    }
}
