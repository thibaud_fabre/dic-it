<?php

namespace DICIT\Injectors;

use DICIT\ArrayResolver;
use DICIT\Container;

class MethodInvoker
{

    /**
     *
     * @param Container $container
     */
    public function invoke(Container $container, $service, $methodName, $parameters)
    {
        $this->injectMethod($container, $service, $methodName, $parameters);
    }

    private function injectMethod(Container $container, $service, $methodName, $parameters)
    {
        $methodToCall = $methodName;

        if ($this->isInInvocationList($methodName)) {
            $methodToCall = $this->extractMethodNameFromList($methodName);
        }

        if ($parameters instanceof ArrayResolver) {
            $parameters = $parameters->extract();
        }

        if (! is_array($parameters)) {
            $parameters = array(
                $parameters
            );
        }

        $convertedParameters = $container->resolveMany($parameters);

        call_user_func_array(array(
            $service,
            $methodToCall
        ), $convertedParameters);
    }

    private function isInInvocationList($methodName)
    {
        return (false !== strpos($methodName, '['));
    }

    private function extractMethodNameFromList($methodName)
    {
        $matches = array();

        if (preg_match('`^([^\[]*)\[[0-9]*\]$`i', $methodName, $matches)) {
            return $matches[1];
        }

        throw new \RuntimeException(sprintf("Invalid method name '%s'", $methodName));
    }
}
