<?php

namespace DICIT\Injectors;

use DICIT\Container;
use DICIT\Injector;

class PropertyInjector implements Injector
{
    public function inject(Container $container, $service, array $serviceConfig)
    {
        $propConfig = array();

        if (array_key_exists('props', $serviceConfig)) {
            $propConfig = $serviceConfig['props'];
        }
        elseif (array_key_exists('properties', $serviceConfig)) {
            $propConfig = $serviceConfig['properties'];
        }

        foreach($propConfig as $propName => $propValue) {
            $convertedValue = $container->resolve($propValue);
            $service->$propName = $convertedValue;
        }

        return true;
    }
}
