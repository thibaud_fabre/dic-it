<?php

namespace DICIT\Resolver;

interface Resolver
{

    function accepts($reference);

    function resolve($reference);
}
