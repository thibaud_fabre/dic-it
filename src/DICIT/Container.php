<?php

namespace DICIT;

interface Container extends ReferenceResolver
{

    public function get($serviceName);

    public function getNamespace($namespace);

    public function getParameter($parameterName);
}
